<?php  

include_once('transporte.php');
	//Declaración de la clase que extiende de transporte
	class Tren extends transporte{

		private $vagones;

		//declaracion de constructor
		public function __construct($nom,$vel,$com,$vag){
			//sobreescritura de constructor de la clase padre
			parent::__construct($nom,$vel,$com);
			$this->vagones=$vag;
				
		}

		// declaracion de metodo
		public function resumenTren(){
			// sobreescribitura de metodo crear_ficha en la clse padre
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Número de vagones:</td>
						<td>'. $this->vagones.'</td>				
					</tr>';
			return $mensaje;
		}
	} 

    $mensaje='';

    if (!empty($_POST)){
        $tren= new Tren('Maya','120','Energía Eléctrica','7');
        $mensaje=$tren->resumenTren();
    }
?>